from django.urls import path, include

from .views import (
    list_techicians,
    list_appointments,
    detail_technician,
    detail_appointment,
)

urlpatterns = [
    path("technicians/", list_techicians, name="list_techicians"),
    path("appointments/", list_appointments, name="list_appointments"),
    path("technician/<int:pk>/", detail_technician, name="detail_technician"),
    path("appointments/<int:pk>/", detail_appointment, name="detail_appointment"),
]
