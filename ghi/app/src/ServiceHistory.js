import React, { useEffect, useState } from "react";

function ServiceHistory({AppointmentList}) {

    const [search, setSearch] = useState("");
    useEffect(() => { }, []);


    return (
        <>
            <h1>
                Service History by VIN
            </h1>
            <div className="container">
                <div className="row">
                    <form id="form_search" name="form_search" method="get" action="" className="form-inline">
                        <div className="form-group">
                            <div className="input-group">
                                <input onChange={event => setSearch(event.target.value)} className="form-control" type="text" placeholder="Search by Vin" />
                            </div>
                        </div>
                    </form>
                </div>
            <table className="bdr table-info table table-striped table-hover">
            <thead>
                <tr>
                <th>VIN</th>
                <th>Customer Name</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                </tr>
            </thead>

            <tbody>
                {AppointmentList && AppointmentList
                .filter(history => history.vin.includes(search))
                .map( history => {
                    return (
                    <tr key={history.id}>
                        <td>{history.vin}</td>
                        <td>{history.customer_name}</td>
                        <td>{history.date}</td>
                        <td>{history.time}</td>
                        <td>{history.technician}</td>
                        <td>{history.reason}</td>
                    </tr>
                    );
                })}
            </tbody>
            </table>
            </div>
        </>
  );
}

export default ServiceHistory;
